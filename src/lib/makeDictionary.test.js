const makeDictionary = require('./makeDictionary');

const en_dict = require('../locale/lang_en/translation.json');
const it_dict = require('../locale/lang_it/translation.json');

const SERIALIZED_DATA = '{"isEmpty":false,"locale":"en","translations":{"_language":"Language","_home":"Homepage","_welcome":"Welcome","_signin":"Login","_signout":"Logout","_signup":"Register","_back":"Back","_next":"Next","_cancel":"Cancel","_confirm":"Confirm","_my_age":"I\'m %s years old"},"supportedLanguages":["en","it"]}';

let dict = null;

test('factory function: empty call', () => {
    dict = makeDictionary();
    expect(dict).not.toBeNull();
    expect(dict.language).toEqual({
        locale: 'en',
        translations: {}
    });
    expect(dict.locale).toEqual('en');
    expect(dict.translations).toEqual({});
    expect(dict.isEmpty).toBe(true);
    expect(dict.supportedLanguages).toEqual(['en']);
});

test('factory function with params', () => {
    dict = makeDictionary(['en', 'it'], 'en', en_dict);
    expect(dict).not.toBeNull();
    expect(dict.language).toEqual({
        locale: 'en',
        translations: en_dict
    });
    expect(dict.locale).toEqual('en');
    expect(dict.translations).toEqual({
        '_language': 'Language',
        '_home': 'Homepage',
        '_welcome': 'Welcome',
        '_signin': 'Login',
        '_signout': 'Logout',
        '_signup': 'Register',
        '_back': 'Back',
        '_next': 'Next',
        '_cancel': 'Cancel',
        '_confirm': 'Confirm',
        '_my_age': "I'm %s years old"
    });
    expect(dict.isEmpty).toBe(false);
    expect(dict.supportedLanguages).toEqual(['en', 'it']);
});

test('hasOwnProperty', () => {
    expect(dict.hasOwnProperty('_name')).toBe(false);
    expect(dict.hasOwnProperty('_confirm')).toBe(true);
});

test('call', () => {
    expect(dict.call(null, '_confirm')).toBe('Confirm');
    expect(dict.call(null, '_my_age', 34)).toBe("I'm 34 years old");
});

test('apply', () => {
    expect(dict.apply(null, ['_confirm'])).toBe('Confirm');
    expect(dict.apply(null, ['_my_age', 34])).toBe("I'm 34 years old");
});

test('serialization', () => {
    expect(dict.toJSON()).toEqual(JSON.parse(SERIALIZED_DATA));
    expect(dict.toString()).toBe(SERIALIZED_DATA);
});

test('deserialization', () => {
    let deserialized = makeDictionary(JSON.parse(SERIALIZED_DATA));
    expect(deserialized).not.toBeNull();
    expect(deserialized.language).toEqual({
        locale: 'en',
        translations: en_dict
    });
    expect(deserialized.locale).toEqual('en');
    expect(deserialized.translations).toEqual({
        '_language': 'Language',
        '_home': 'Homepage',
        '_welcome': 'Welcome',
        '_signin': 'Login',
        '_signout': 'Logout',
        '_signup': 'Register',
        '_back': 'Back',
        '_next': 'Next',
        '_cancel': 'Cancel',
        '_confirm': 'Confirm',
        '_my_age': "I'm %s years old"
    });
    expect(deserialized.isEmpty).toBe(false);
    expect(deserialized.supportedLanguages).toEqual(['en', 'it']);
});

test('translate values', () => {
    expect(dict._next).toBe('Next');
    expect(dict('_next')).toBe('Next');
    expect(dict('_my_age')).toBe('I\'m %s years old');
    expect(dict('_my_age', 34)).toBe('I\'m 34 years old');
});


test('fallback values', () => {
    expect(dict._key_not_in_translations).toBe('_key_not_in_translations');
    expect(dict('_key_not_in_translations')).toBe('_key_not_in_translations');
    expect(dict('_key_not_in_translations', 69)).toBe('_key_not_in_translations');
});


test('switch language', () => {
    dict.language = {
        locale: 'it',
        translations: it_dict
    };

    expect(dict.language).toEqual({
        locale: 'it',
        translations: it_dict
    });
    expect(dict.locale).toEqual('it');
    expect(dict.translations).toEqual({
        '_language': 'Lingua',
        '_home': 'Home',
        '_welcome': 'Benvenuto',
        '_signin': 'Login',
        '_signout': 'Logout',
        '_signup': 'Registrati',
        '_back': 'Indietro',
        '_next': 'Avanti',
        '_cancel': 'Annulla',
        '_confirm': 'Conferma',
        '_my_age': 'Ho %s anni'
    });
    expect(dict.isEmpty).toBe(false);
    expect(dict._next).toBe('Avanti');
    expect(dict('_next')).toBe('Avanti');
    expect(dict('_my_age')).toBe('Ho %s anni');
    expect(dict('_my_age', 34)).toBe('Ho 34 anni');
});

test('unsupported language', () => {
    // why this throws in strict mode https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/handler/set#Return_value
    expect(() => { dict.language = 'es' }).toThrowError("'set' on proxy: trap returned falsish for property 'language'");
    expect(dict.locale).toBe('it');
});