/**
 * Accepts either 1 or 2 arguments
 * 1 argument: must be {locale: locale, translations: translations}
 * 2 arguments: locale, translations
 * locale: 2 charater string of the locale code
 * translations: JavaScript plain object where all values are strings (can be templated using sprintf convention)
 * keys should begin with _ or __
 */
class LanguageDefinition {
    constructor() {
        let err = null;
        let _locale = null,
            _translations = null;
        switch (arguments.length) {
            case 0:
                err = new Error('Either use {locale:locale, translation:translation} or locale, translation for the constructor!');
                break;
            case 1:
                //Got an object
                _locale = arguments[0].locale;
                _translations = arguments[0].translations;
                if (!_locale || !_translations) {
                    err = new Error('Config object must define locale and translations property');
                }
                break;
            case 2:
                _locale = arguments[0];
                _translations = arguments[1];
                break;
            default:
                err = new Error('This class takes exactly 1 or 2 arguments');
                break;
        }
        if (_locale && !this.validateLanguage(_locale)) {
            err = new Error('Invalid locale');
        }
        if (_translations && !this.validateTranslate(_translations)) {
            err = new Error('Invalid translations object');
        }
        if (err) {
            throw err;
        } else {
            Object.defineProperties(this, {
                locale: {
                    enumerable: true,
                    configurable: false,
                    writable: false,
                    value: _locale
                },
                translations: {
                    enumerable: true,
                    configurable: false,
                    writable: false,
                    value: _translations
                }
            });
        }
    }

    validateLanguage(locale) {
        return typeof locale === 'string' && locale.length === 2;
    }

    validateTranslate(translations) {
        return Object.getPrototypeOf(translations).toString() === '[object Object]' && Object.values(translations).filter((v) => {
                return typeof v !== 'string';
            }).length === 0;
    }
}

module.exports = LanguageDefinition;
module.exports.default = LanguageDefinition;