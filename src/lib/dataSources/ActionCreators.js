import DefaultState from './DefaultState';
import { 
    LOCALE_CHANGED,
    SET_SUPPORTED_LANGUAGES,
    DICT_SET,
    DICT_EXTEND,
    DICT_RESET
} from './Actions';

function _setLocale(locale, translations) {
    return {
        type: LOCALE_CHANGED,
        locale,
        translations
    };
}

function _extendDictionary(translations) {
    return {
        type: DICT_EXTEND,
        translations
    };
}

/**
 * Extend the translation loaded by localeLoader to the current dictionary
 * @param {Function} localeLoader - asyncronously load a dictionary
 */
export function extendDictionary(localeLoader = emptyLoader){
    return async dispatch => {
        localeLoader((translations) => {
            dispatch(_extendDictionary(translations));
        });
    };
}

function _setDictionary(translations) {
    return {
        type: DICT_SET,
        translations
    };
}

/**
 * Set the translation loaded by localeLoader as the current dictionary
 * @param {Function} localeLoader - asyncronously load a dictionary
 */
export function setDictionary(localeLoader = emptyLoader){
    return async dispatch => {
        localeLoader((translations) => {
            dispatch(_setDictionary(translations));
        });
    };
}

/**
 * Clear the current dictionary without changing locale
 */
export function resetDictionary() {
    return {
        type: DICT_RESET
    };
}

/**
 * Set the currently supported language codes
 * @param {Array} supportedLanguages - Array of iso strings of language codes
 */
export function setSupportedLanguages(supportedLanguages) {
    return {
        type: SET_SUPPORTED_LANGUAGES,
        supportedLanguages: supportedLanguages
    };
}

/**
 * Set the lang as the document language
 * @param {String} lang - language code 
 */
function changeDocumentLocale(lang) {
    if (document) {
        const html = document.querySelector('html');
        if (html) {
            html.setAttribute('lang', lang);
        }
    }
}

function emptyLoader(cb) {
    cb({});
}

/**
 * Load a new dictionary setting also the locale
 * @param {String} locale 
 * @param {Function} localeLoader - asyncronously load a dictionary
 */
export function loadDictionary(locale = DefaultState.locale, localeLoader = emptyLoader) {
    return async dispatch => {
        localeLoader((translations) => {
            dispatch(_setLocale(locale, translations));
            changeDocumentLocale(locale);
        });
    };
}
