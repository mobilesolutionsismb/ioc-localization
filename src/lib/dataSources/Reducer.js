import DefaultState from "./DefaultState";
import {
  LOCALE_CHANGED,
  SET_SUPPORTED_LANGUAGES,
  DICT_SET,
  DICT_EXTEND,
  DICT_RESET
} from "./Actions";
import { reduceWith } from "redux-reduce-with";

const mutators = {
  // Load dictionary and change locale
  [LOCALE_CHANGED]: {
    locale: action => action.locale,
    dictVersion: 0,
    dictionary: (action, state) => {
      return {
        translations: action.translations,
        locale: action.locale,
        isEmpty: Object.keys(action.translations).length === 0,
        supportedLanguages: state.dictionary.supportedLanguages
      };
    }
  },
  // Change translations without changing locale
  [DICT_SET]: {
    dictVersion: 0,
    dictionary: (action, state) => {
      return {
        translations: action.translations,
        locale: action.locale,
        isEmpty: Object.keys(action.translations).length === 0,
        supportedLanguages: state.dictionary.supportedLanguages
      };
    }
  },
  // Extend translations without changing locale - will override existing keys!
  [DICT_EXTEND]: {
    dictVersion: (action, state) => state.locale.version + 1,
    dictionary: (action, state) => {
      const dictMerge = {
        ...action.translations,
        ...state.dictionary.translations
      };
      return {
        translations: dictMerge,
        locale: state.dictionary.locale,
        isEmpty: Object.keys(dictMerge).length === 0,
        supportedLanguages: state.dictionary.supportedLanguages
      };
    }
  },
  // Clear dictionary
  [DICT_RESET]: {
    dictVersion: 0,
    dictionary: (action, state) => {
      return {
        translations: {},
        locale: state.dictionary.locale,
        isEmpty: true,
        supportedLanguages: state.dictionary.supportedLanguages
      };
    }
  },
  // Set supported languages
  [SET_SUPPORTED_LANGUAGES]: {
    dictVersion: 0,
    supportedLanguages: action => action.supportedLanguages,
    dictionary: (action, state) => {
      return {
        translations: state.dictionary.translations,
        locale: state.dictionary.locale,
        isEmpty: state.dictionary.isEmpty,
        supportedLanguages: Object.keys(action.supportedLanguages)
      };
    }
  }
};

export default reduceWith(mutators, DefaultState);
