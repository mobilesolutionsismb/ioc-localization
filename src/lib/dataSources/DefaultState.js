const _SUPPORTED_LANGUAGES = typeof SUPPORTED_LANGUAGES === 'undefined' ? require('locale/languages.json') : SUPPORTED_LANGUAGES;

const supportedLanguages = Object.keys(_SUPPORTED_LANGUAGES);
const FALLBACK_LANGUAGE = supportedLanguages[0] || 'en';
const SYSTEM_LANGUAGE = typeof window.navigator.language === 'string' ? window.navigator.language.substr(0, 2).toLowerCase() : null;

const DEFAULT_LOCALE = supportedLanguages.indexOf(SYSTEM_LANGUAGE) === -1 ? FALLBACK_LANGUAGE : SYSTEM_LANGUAGE;

const html = document.querySelector('html');
if (html) {
    html.setAttribute('lang', DEFAULT_LOCALE);
}

const STATE = {
    locale: DEFAULT_LOCALE,
    dictVersion: 0,
    dictionary: {
        isEmpty: true,
        locale: DEFAULT_LOCALE,
        translations: {},
        supportedLanguages: supportedLanguages
    },
    supportedLanguages: _SUPPORTED_LANGUAGES
};

export default STATE;
