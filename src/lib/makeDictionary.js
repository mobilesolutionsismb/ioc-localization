const sprintf = require('sprintf-js').sprintf;
const LanguageDefinition = require('./LanguageDefinition');

function translationsIsEmpty(translations) {
    return Object.keys(translations).length === 0;
}

const TranslationsHandler = {
    set: function(target, propName, value) {
        /**
         * Switch language by passing a config object, e.g.
         * {locale: 'en', translations: {_hello: "Hello!"}}
         * WARNING: throws TypeError in strict mode if result is false (language unsupported)
         * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/handler/set#Return_value
         */
        let result = false;
        const isValidPropertyName = propName === 'language' && Object.getPrototypeOf(value).toString() === '[object Object]' && value.hasOwnProperty('locale') && value.hasOwnProperty('translations');
        if (isValidPropertyName) {
            try {
                target[propName] = new LanguageDefinition(value);
                result = true;
            } catch (ex) {
                console.warn('Invalid language', value, ex);
            }
        }
        return result;
    },
    get: function(target, key) {
        let result;
        switch (key) {
            case 'apply':
                result = (thisArg, argumentsList) => {
                    const translations = target.language.translations;
                    if (argumentsList.length < 1) {
                        return null;
                    } else if (argumentsList.length === 1) {
                        const key = argumentsList[0];
                        return translations.hasOwnProperty(key) ? translations[key] : key;
                    } else {
                        const key = argumentsList[0];
                        const args = argumentsList.slice(1, argumentsList.length);
                        return translations.hasOwnProperty(key) ? sprintf.apply(null, [translations[key], ...args]) : key;
                    }
                };
                break;
            case 'call':
                result = (thisArg, ...argumentsList) => {
                    const translations = target.language.translations;
                    if (argumentsList.length < 1) {
                        return null;
                    } else if (argumentsList.length === 1) {
                        const key = argumentsList[0];
                        return translations.hasOwnProperty(key) ? translations[key] : key;
                    } else {
                        const key = argumentsList[0];
                        const args = argumentsList.slice(1, argumentsList.length);
                        return translations.hasOwnProperty(key) ? sprintf.apply(null, [translations[key], ...args]) : key;
                    }
                };
                break;
            case 'hasOwnProperty':
                result = (val) => target.language.translations.hasOwnProperty(val);
                break;
            case 'toString':
                result = () => {
                    const pojo = {
                        isEmpty: translationsIsEmpty(target.language.translations),
                        locale: target.language.locale,
                        translations: target.language.translations,
                        supportedLanguages: target.supportedLanguages
                    };
                    return JSON.stringify(pojo);
                };
                break;
            case 'toJSON':
                result = () => {
                    const pojo = {
                        isEmpty: translationsIsEmpty(target.language.translations),
                        locale: target.language.locale,
                        translations: target.language.translations,
                        supportedLanguages: target.supportedLanguages
                    };
                    return pojo;
                };
                break;
            case 'isEmpty':
                result = translationsIsEmpty(target.language.translations);
                break;
            case 'language':
                result = {
                    locale: target.language.locale,
                    translations: target.language.translations
                };
                break;
            case 'locale':
            case 'translations':
                result = target.language[key];
                break;
            case 'supportedLanguages':
                result = target.supportedLanguages;
                break;
            default:
                if (typeof key === 'string') {
                    result = target.language.translations.hasOwnProperty(key) ? target.language.translations[key] : key;
                } else {
                    result = target.language.translations;
                }
                break;
        }
        return result;
    },
    apply: function(target, thisArg, argumentsList) {
        let translations = target.language.translations;
        if (argumentsList.length < 1) {
            return null;
        } else if (argumentsList.length === 1) {
            let key = argumentsList[0];
            return translations.hasOwnProperty(key) ? translations[key] : key;
        } else {
            let key = argumentsList[0];
            let args = argumentsList.slice(1, argumentsList.length);
            return translations.hasOwnProperty(key) ? sprintf.apply(null, [translations[key], ...args]) : key;
        }
    }
};

const DEFAULT_VALUES = {
    supportedLanguages: ['en'],
    language: 'en',
    translations: {},
    isEmpty: true
};

function makeDictionary(...args) {
    let {supportedLanguages, language, translations, isEmpty} = DEFAULT_VALUES;
    switch (args.length) {
        case 3:
            supportedLanguages = args[0];
            language = args[1];
            translations = args[2];
            break;
        case 2:
            supportedLanguages = args[0];
            language = args[1];
            break;
        case 1:
            if (!Array.isArray(args[0]) && args[0].hasOwnProperty('supportedLanguages') && args[0].hasOwnProperty('translations') && args[0].hasOwnProperty('locale')) {
                supportedLanguages = args[0].supportedLanguages;
                language = args[0].locale;
                translations = args[0].translations;
            } else {
                supportedLanguages = args[0];
            }
            break;
        case 0:
        default:
            break;
    }

    let target = function() {};
    if (!Array.isArray(supportedLanguages) || supportedLanguages.length === 0) {
        throw new Error('supportedLanguages must be a non-empty Array of 2-characters language codes');
    } else {
        target.supportedLanguages = supportedLanguages;
    }

    if (typeof language === 'string') {
        target.language = new LanguageDefinition(language, translations);
    } else {
        //assuming language is an object with property locale and translations
        target.language = new LanguageDefinition(language);
    }
    target.isEmpty = translationsIsEmpty(target.language.translations);

    const p = new Proxy(target, TranslationsHandler);
    return p;
}

module.exports = makeDictionary;
module.exports.default = makeDictionary;
