import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

import DefaultState from "../dataSources/DefaultState";
import * as ActionCreators from "../dataSources/ActionCreators";

import makeDictionary from "../makeDictionary";
const translator = makeDictionary(DefaultState.dictionary);

const localeMapState = state => state.locale.locale;
const dictVersionMapState = state => state.locale.dictVersion;
const dictMapState = state => {
  const dict = state.locale.dictionary;
  translator.language = {
    locale: dict.locale,
    translations: dict.translations
  };
  return translator;
};
const dictIsEmptyMapState = state => state.locale.dictionary.isEmpty;

const supportedLanguagesMapState = state => state.locale.supportedLanguages;

export const localeSelector = createStructuredSelector({
  locale: localeMapState,
  dictVersion: dictVersionMapState,
  dictionary: dictMapState,
  dictIsEmpty: dictIsEmptyMapState,
  supportedLanguages: supportedLanguagesMapState
});

export { ActionCreators as localeActionCreators };

export default connect(
  localeSelector,
  ActionCreators
);
