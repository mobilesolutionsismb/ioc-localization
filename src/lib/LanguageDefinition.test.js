const LanguageDefinition = require('./LanguageDefinition');

const en_dict = require('../locale/lang_en/translation.json');
const it_dict = require('../locale/lang_it/translation.json');

test('constructor', () => {
    let english = new LanguageDefinition({
        locale: 'en',
        translations: en_dict
    });
    expect(english).not.toBeNull();
    expect(english.locale).toBe('en');
    expect(english.translations).toEqual({
        '_language': 'Language',
        '_home': 'Homepage',
        '_welcome': 'Welcome',
        '_signin': 'Login',
        '_signout': 'Logout',
        '_signup': 'Register',
        '_back': 'Back',
        '_next': 'Next',
        '_cancel': 'Cancel',
        '_confirm': 'Confirm',
        '_my_age': "I'm %s years old"
    });

    let italian = new LanguageDefinition('it', it_dict);
    expect(italian).not.toBeNull();
    expect(italian.locale).toBe('it');
    expect(italian.translations).toEqual({
        '_language': 'Lingua',
        '_home': 'Home',
        '_welcome': 'Benvenuto',
        '_signin': 'Login',
        '_signout': 'Logout',
        '_signup': 'Registrati',
        '_back': 'Indietro',
        '_next': 'Avanti',
        '_cancel': 'Annulla',
        '_confirm': 'Conferma',
        '_my_age': 'Ho %s anni'
    });
});

test('constructor: invalid args', () => {
    let noArgs = () => {
        try {
            let ld = new LanguageDefinition();
        } catch (ex) {
            throw ex;
        }
    };

    let wrongLocale = () => {
        try {
            let ld = new LanguageDefinition('italiano', {
                _greet: 'ciao'
            });
        } catch (ex) {
            throw ex;
        }
    };

    let wrongTranslation = () => {
        try {
            let ld = new LanguageDefinition('es', {
                _my_key: 99
            });
        } catch (ex) {
            throw ex;
        }
    };

    expect(noArgs).toThrowError('Either use {locale:locale, translation:translation} or locale, translation for the constructor!');
    expect(wrongLocale).toThrowError('Invalid locale');
    expect(wrongTranslation).toThrowError('Invalid translations object');
});