import Reducer from './lib/dataSources/Reducer';
export * from './lib/dataSources/ActionCreators';
export { default as withDictionary, localeActionCreators, localeSelector } from './lib/dataProviders/withDictionary';
export { default as makeDictionary } from './lib/makeDictionary';

export default {
    name: 'locale',
    reducer: Reducer
};

export { Reducer };