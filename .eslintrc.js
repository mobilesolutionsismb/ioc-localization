module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true,
        "node": true,
        "jest": true
    },
    "plugins": [
        "jest"
    ],
    "parser": "babel-eslint",
    "extends": "eslint:recommended",
    "installedESLint": true,
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true
        },
        "sourceType": "module"
    },
    "globals": {
        //Global Config
        "TITLE": true,
        "VERSION": true,
        "PKG_NAME": true,
        "DESCRIPTION": true,
        "ENVIRONMENT": true,
        "BUILD_DATE": true,
        "SUPPORTED_LANGUAGES": true
    },
    "rules": {
        "jest/no-exclusive-tests": 2,
        "jest/no-identical-title": 2,
        "no-unused-vars": "off",
        "no-console": "off",
        "no-fallthrough": "off",
        "no-inner-declarations": "off",
        "indent": [
            "off",
            2
        ],
        "linebreak-style": [
            "off"
        ],
        "quotes": [
            "error",
            "single",
            {
                "avoidEscape": true
            }
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};
