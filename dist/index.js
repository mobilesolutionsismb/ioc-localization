(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("react-redux"), require("reselect"), require("sprintf-js"), require("redux-reduce-with"));
	else if(typeof define === 'function' && define.amd)
		define(["react-redux", "reselect", "sprintf-js", "redux-reduce-with"], factory);
	else if(typeof exports === 'object')
		exports["ismb-react-redux-locale"] = factory(require("react-redux"), require("reselect"), require("sprintf-js"), require("redux-reduce-with"));
	else
		root["ismb-react-redux-locale"] = factory(root["react-redux"], root["reselect"], root["sprintf-js"], root["redux-reduce-with"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_8__, __WEBPACK_EXTERNAL_MODULE_9__, __WEBPACK_EXTERNAL_MODULE_10__, __WEBPACK_EXTERNAL_MODULE_13__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var _SUPPORTED_LANGUAGES = typeof SUPPORTED_LANGUAGES === 'undefined' ? __webpack_require__(6) : SUPPORTED_LANGUAGES;

var supportedLanguages = Object.keys(_SUPPORTED_LANGUAGES);
var FALLBACK_LANGUAGE = supportedLanguages[0] || 'en';
var SYSTEM_LANGUAGE = typeof window.navigator.language === 'string' ? window.navigator.language.substr(0, 2).toLowerCase() : null;

var DEFAULT_LOCALE = supportedLanguages.indexOf(SYSTEM_LANGUAGE) === -1 ? FALLBACK_LANGUAGE : SYSTEM_LANGUAGE;

var html = document.querySelector('html');
if (html) {
    html.setAttribute('lang', DEFAULT_LOCALE);
}

var STATE = {
    locale: DEFAULT_LOCALE,
    dictVersion: 0,
    dictionary: {
        isEmpty: true,
        locale: DEFAULT_LOCALE,
        translations: {},
        supportedLanguages: supportedLanguages
    },
    supportedLanguages: _SUPPORTED_LANGUAGES
};

exports.default = STATE;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.extendDictionary = extendDictionary;
exports.setDictionary = setDictionary;
exports.resetDictionary = resetDictionary;
exports.setSupportedLanguages = setSupportedLanguages;
exports.loadDictionary = loadDictionary;

var _DefaultState = __webpack_require__(0);

var _DefaultState2 = _interopRequireDefault(_DefaultState);

var _Actions = __webpack_require__(2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _setLocale(locale, translations) {
    return {
        type: _Actions.LOCALE_CHANGED,
        locale: locale,
        translations: translations
    };
}

function _extendDictionary(translations) {
    return {
        type: _Actions.DICT_EXTEND,
        translations: translations
    };
}

/**
 * Extend the translation loaded by localeLoader to the current dictionary
 * @param {Function} localeLoader - asyncronously load a dictionary
 */
function extendDictionary() {
    var localeLoader = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : emptyLoader;

    return async function (dispatch) {
        localeLoader(function (translations) {
            dispatch(_extendDictionary(translations));
        });
    };
}

function _setDictionary(translations) {
    return {
        type: _Actions.DICT_SET,
        translations: translations
    };
}

/**
 * Set the translation loaded by localeLoader as the current dictionary
 * @param {Function} localeLoader - asyncronously load a dictionary
 */
function setDictionary() {
    var localeLoader = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : emptyLoader;

    return async function (dispatch) {
        localeLoader(function (translations) {
            dispatch(_setDictionary(translations));
        });
    };
}

/**
 * Clear the current dictionary without changing locale
 */
function resetDictionary() {
    return {
        type: _Actions.DICT_RESET
    };
}

/**
 * Set the currently supported language codes
 * @param {Array} supportedLanguages - Array of iso strings of language codes
 */
function setSupportedLanguages(supportedLanguages) {
    return {
        type: _Actions.SET_SUPPORTED_LANGUAGES,
        supportedLanguages: supportedLanguages
    };
}

/**
 * Set the lang as the document language
 * @param {String} lang - language code 
 */
function changeDocumentLocale(lang) {
    if (document) {
        var html = document.querySelector('html');
        if (html) {
            html.setAttribute('lang', lang);
        }
    }
}

function emptyLoader(cb) {
    cb({});
}

/**
 * Load a new dictionary setting also the locale
 * @param {String} locale 
 * @param {Function} localeLoader - asyncronously load a dictionary
 */
function loadDictionary() {
    var locale = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _DefaultState2.default.locale;
    var localeLoader = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : emptyLoader;

    return async function (dispatch) {
        localeLoader(function (translations) {
            dispatch(_setLocale(locale, translations));
            changeDocumentLocale(locale);
        });
    };
}

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var NS = "LOCALE";

var LOCALE_CHANGED = NS + "@LOCALE_CHANGED";
var DICT_SET = NS + "@DICT_SET";
var DICT_EXTEND = NS + "@DICT_EXTEND";
var DICT_RESET = NS + "@DICT_RESET";
var SET_SUPPORTED_LANGUAGES = NS + "@SET_SUPPORTED_LANGUAGES";

exports.LOCALE_CHANGED = LOCALE_CHANGED;
exports.SET_SUPPORTED_LANGUAGES = SET_SUPPORTED_LANGUAGES;
exports.DICT_SET = DICT_SET;
exports.DICT_EXTEND = DICT_EXTEND;
exports.DICT_RESET = DICT_RESET;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var sprintf = __webpack_require__(10).sprintf;
var LanguageDefinition = __webpack_require__(11);

function translationsIsEmpty(translations) {
    return Object.keys(translations).length === 0;
}

var TranslationsHandler = {
    set: function set(target, propName, value) {
        /**
         * Switch language by passing a config object, e.g.
         * {locale: 'en', translations: {_hello: "Hello!"}}
         * WARNING: throws TypeError in strict mode if result is false (language unsupported)
         * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/handler/set#Return_value
         */
        var result = false;
        var isValidPropertyName = propName === 'language' && Object.getPrototypeOf(value).toString() === '[object Object]' && value.hasOwnProperty('locale') && value.hasOwnProperty('translations');
        if (isValidPropertyName) {
            try {
                target[propName] = new LanguageDefinition(value);
                result = true;
            } catch (ex) {
                console.warn('Invalid language', value, ex);
            }
        }
        return result;
    },
    get: function get(target, key) {
        var result = void 0;
        switch (key) {
            case 'apply':
                result = function result(thisArg, argumentsList) {
                    var translations = target.language.translations;
                    if (argumentsList.length < 1) {
                        return null;
                    } else if (argumentsList.length === 1) {
                        var _key = argumentsList[0];
                        return translations.hasOwnProperty(_key) ? translations[_key] : _key;
                    } else {
                        var _key2 = argumentsList[0];
                        var args = argumentsList.slice(1, argumentsList.length);
                        return translations.hasOwnProperty(_key2) ? sprintf.apply(null, [translations[_key2]].concat(_toConsumableArray(args))) : _key2;
                    }
                };
                break;
            case 'call':
                result = function result(thisArg) {
                    for (var _len = arguments.length, argumentsList = Array(_len > 1 ? _len - 1 : 0), _key3 = 1; _key3 < _len; _key3++) {
                        argumentsList[_key3 - 1] = arguments[_key3];
                    }

                    var translations = target.language.translations;
                    if (argumentsList.length < 1) {
                        return null;
                    } else if (argumentsList.length === 1) {
                        var _key4 = argumentsList[0];
                        return translations.hasOwnProperty(_key4) ? translations[_key4] : _key4;
                    } else {
                        var _key5 = argumentsList[0];
                        var args = argumentsList.slice(1, argumentsList.length);
                        return translations.hasOwnProperty(_key5) ? sprintf.apply(null, [translations[_key5]].concat(_toConsumableArray(args))) : _key5;
                    }
                };
                break;
            case 'hasOwnProperty':
                result = function result(val) {
                    return target.language.translations.hasOwnProperty(val);
                };
                break;
            case 'toString':
                result = function result() {
                    var pojo = {
                        isEmpty: translationsIsEmpty(target.language.translations),
                        locale: target.language.locale,
                        translations: target.language.translations,
                        supportedLanguages: target.supportedLanguages
                    };
                    return JSON.stringify(pojo);
                };
                break;
            case 'toJSON':
                result = function result() {
                    var pojo = {
                        isEmpty: translationsIsEmpty(target.language.translations),
                        locale: target.language.locale,
                        translations: target.language.translations,
                        supportedLanguages: target.supportedLanguages
                    };
                    return pojo;
                };
                break;
            case 'isEmpty':
                result = translationsIsEmpty(target.language.translations);
                break;
            case 'language':
                result = {
                    locale: target.language.locale,
                    translations: target.language.translations
                };
                break;
            case 'locale':
            case 'translations':
                result = target.language[key];
                break;
            case 'supportedLanguages':
                result = target.supportedLanguages;
                break;
            default:
                if (typeof key === 'string') {
                    result = target.language.translations.hasOwnProperty(key) ? target.language.translations[key] : key;
                } else {
                    result = target.language.translations;
                }
                break;
        }
        return result;
    },
    apply: function apply(target, thisArg, argumentsList) {
        var translations = target.language.translations;
        if (argumentsList.length < 1) {
            return null;
        } else if (argumentsList.length === 1) {
            var key = argumentsList[0];
            return translations.hasOwnProperty(key) ? translations[key] : key;
        } else {
            var _key6 = argumentsList[0];
            var args = argumentsList.slice(1, argumentsList.length);
            return translations.hasOwnProperty(_key6) ? sprintf.apply(null, [translations[_key6]].concat(_toConsumableArray(args))) : _key6;
        }
    }
};

var DEFAULT_VALUES = {
    supportedLanguages: ['en'],
    language: 'en',
    translations: {},
    isEmpty: true
};

function makeDictionary() {
    var supportedLanguages = DEFAULT_VALUES.supportedLanguages,
        language = DEFAULT_VALUES.language,
        translations = DEFAULT_VALUES.translations,
        isEmpty = DEFAULT_VALUES.isEmpty;

    for (var _len2 = arguments.length, args = Array(_len2), _key7 = 0; _key7 < _len2; _key7++) {
        args[_key7] = arguments[_key7];
    }

    switch (args.length) {
        case 3:
            supportedLanguages = args[0];
            language = args[1];
            translations = args[2];
            break;
        case 2:
            supportedLanguages = args[0];
            language = args[1];
            break;
        case 1:
            if (!Array.isArray(args[0]) && args[0].hasOwnProperty('supportedLanguages') && args[0].hasOwnProperty('translations') && args[0].hasOwnProperty('locale')) {
                supportedLanguages = args[0].supportedLanguages;
                language = args[0].locale;
                translations = args[0].translations;
            } else {
                supportedLanguages = args[0];
            }
            break;
        case 0:
        default:
            break;
    }

    var target = function target() {};
    if (!Array.isArray(supportedLanguages) || supportedLanguages.length === 0) {
        throw new Error('supportedLanguages must be a non-empty Array of 2-characters language codes');
    } else {
        target.supportedLanguages = supportedLanguages;
    }

    if (typeof language === 'string') {
        target.language = new LanguageDefinition(language, translations);
    } else {
        //assuming language is an object with property locale and translations
        target.language = new LanguageDefinition(language);
    }
    target.isEmpty = translationsIsEmpty(target.language.translations);

    var p = new Proxy(target, TranslationsHandler);
    return p;
}

module.exports = makeDictionary;
module.exports.default = makeDictionary;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(5);


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Reducer = exports.makeDictionary = exports.localeSelector = exports.localeActionCreators = exports.withDictionary = undefined;

var _ActionCreators = __webpack_require__(1);

Object.keys(_ActionCreators).forEach(function (key) {
    if (key === "default" || key === "__esModule") return;
    Object.defineProperty(exports, key, {
        enumerable: true,
        get: function get() {
            return _ActionCreators[key];
        }
    });
});

var _withDictionary = __webpack_require__(7);

Object.defineProperty(exports, 'withDictionary', {
    enumerable: true,
    get: function get() {
        return _interopRequireDefault(_withDictionary).default;
    }
});
Object.defineProperty(exports, 'localeActionCreators', {
    enumerable: true,
    get: function get() {
        return _withDictionary.localeActionCreators;
    }
});
Object.defineProperty(exports, 'localeSelector', {
    enumerable: true,
    get: function get() {
        return _withDictionary.localeSelector;
    }
});

var _makeDictionary = __webpack_require__(3);

Object.defineProperty(exports, 'makeDictionary', {
    enumerable: true,
    get: function get() {
        return _interopRequireDefault(_makeDictionary).default;
    }
});

var _Reducer = __webpack_require__(12);

var _Reducer2 = _interopRequireDefault(_Reducer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    name: 'locale',
    reducer: _Reducer2.default
};
exports.Reducer = _Reducer2.default;

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = {"en":"English","it":"Italiano"}

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.localeActionCreators = exports.localeSelector = undefined;

var _reactRedux = __webpack_require__(8);

var _reselect = __webpack_require__(9);

var _DefaultState = __webpack_require__(0);

var _DefaultState2 = _interopRequireDefault(_DefaultState);

var _ActionCreators = __webpack_require__(1);

var ActionCreators = _interopRequireWildcard(_ActionCreators);

var _makeDictionary = __webpack_require__(3);

var _makeDictionary2 = _interopRequireDefault(_makeDictionary);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var translator = (0, _makeDictionary2.default)(_DefaultState2.default.dictionary);

var localeMapState = function localeMapState(state) {
  return state.locale.locale;
};
var dictVersionMapState = function dictVersionMapState(state) {
  return state.locale.dictVersion;
};
var dictMapState = function dictMapState(state) {
  var dict = state.locale.dictionary;
  translator.language = {
    locale: dict.locale,
    translations: dict.translations
  };
  return translator;
};
var dictIsEmptyMapState = function dictIsEmptyMapState(state) {
  return state.locale.dictionary.isEmpty;
};

var supportedLanguagesMapState = function supportedLanguagesMapState(state) {
  return state.locale.supportedLanguages;
};

var localeSelector = exports.localeSelector = (0, _reselect.createStructuredSelector)({
  locale: localeMapState,
  dictVersion: dictVersionMapState,
  dictionary: dictMapState,
  dictIsEmpty: dictIsEmptyMapState,
  supportedLanguages: supportedLanguagesMapState
});

exports.localeActionCreators = ActionCreators;
exports.default = (0, _reactRedux.connect)(localeSelector, ActionCreators);

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_8__;

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_9__;

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_10__;

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Accepts either 1 or 2 arguments
 * 1 argument: must be {locale: locale, translations: translations}
 * 2 arguments: locale, translations
 * locale: 2 charater string of the locale code
 * translations: JavaScript plain object where all values are strings (can be templated using sprintf convention)
 * keys should begin with _ or __
 */
var LanguageDefinition = function () {
    function LanguageDefinition() {
        _classCallCheck(this, LanguageDefinition);

        var err = null;
        var _locale = null,
            _translations = null;
        switch (arguments.length) {
            case 0:
                err = new Error('Either use {locale:locale, translation:translation} or locale, translation for the constructor!');
                break;
            case 1:
                //Got an object
                _locale = arguments[0].locale;
                _translations = arguments[0].translations;
                if (!_locale || !_translations) {
                    err = new Error('Config object must define locale and translations property');
                }
                break;
            case 2:
                _locale = arguments[0];
                _translations = arguments[1];
                break;
            default:
                err = new Error('This class takes exactly 1 or 2 arguments');
                break;
        }
        if (_locale && !this.validateLanguage(_locale)) {
            err = new Error('Invalid locale');
        }
        if (_translations && !this.validateTranslate(_translations)) {
            err = new Error('Invalid translations object');
        }
        if (err) {
            throw err;
        } else {
            Object.defineProperties(this, {
                locale: {
                    enumerable: true,
                    configurable: false,
                    writable: false,
                    value: _locale
                },
                translations: {
                    enumerable: true,
                    configurable: false,
                    writable: false,
                    value: _translations
                }
            });
        }
    }

    _createClass(LanguageDefinition, [{
        key: 'validateLanguage',
        value: function validateLanguage(locale) {
            return typeof locale === 'string' && locale.length === 2;
        }
    }, {
        key: 'validateTranslate',
        value: function validateTranslate(translations) {
            return Object.getPrototypeOf(translations).toString() === '[object Object]' && Object.values(translations).filter(function (v) {
                return typeof v !== 'string';
            }).length === 0;
        }
    }]);

    return LanguageDefinition;
}();

module.exports = LanguageDefinition;
module.exports.default = LanguageDefinition;

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mutators;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _DefaultState = __webpack_require__(0);

var _DefaultState2 = _interopRequireDefault(_DefaultState);

var _Actions = __webpack_require__(2);

var _reduxReduceWith = __webpack_require__(13);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var mutators = (_mutators = {}, _defineProperty(_mutators, _Actions.LOCALE_CHANGED, {
  locale: function locale(action) {
    return action.locale;
  },
  dictVersion: 0,
  dictionary: function dictionary(action, state) {
    return {
      translations: action.translations,
      locale: action.locale,
      isEmpty: Object.keys(action.translations).length === 0,
      supportedLanguages: state.dictionary.supportedLanguages
    };
  }
}), _defineProperty(_mutators, _Actions.DICT_SET, {
  dictVersion: 0,
  dictionary: function dictionary(action, state) {
    return {
      translations: action.translations,
      locale: action.locale,
      isEmpty: Object.keys(action.translations).length === 0,
      supportedLanguages: state.dictionary.supportedLanguages
    };
  }
}), _defineProperty(_mutators, _Actions.DICT_EXTEND, {
  dictVersion: function dictVersion(action, state) {
    return state.locale.version + 1;
  },
  dictionary: function dictionary(action, state) {
    var dictMerge = _extends({}, action.translations, state.dictionary.translations);
    return {
      translations: dictMerge,
      locale: state.dictionary.locale,
      isEmpty: Object.keys(dictMerge).length === 0,
      supportedLanguages: state.dictionary.supportedLanguages
    };
  }
}), _defineProperty(_mutators, _Actions.DICT_RESET, {
  dictVersion: 0,
  dictionary: function dictionary(action, state) {
    return {
      translations: {},
      locale: state.dictionary.locale,
      isEmpty: true,
      supportedLanguages: state.dictionary.supportedLanguages
    };
  }
}), _defineProperty(_mutators, _Actions.SET_SUPPORTED_LANGUAGES, {
  dictVersion: 0,
  supportedLanguages: function supportedLanguages(action) {
    return action.supportedLanguages;
  },
  dictionary: function dictionary(action, state) {
    return {
      translations: state.dictionary.translations,
      locale: state.dictionary.locale,
      isEmpty: state.dictionary.isEmpty,
      supportedLanguages: Object.keys(action.supportedLanguages)
    };
  }
}), _mutators);

exports.default = (0, _reduxReduceWith.reduceWith)(mutators, _DefaultState2.default);

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_13__;

/***/ })
/******/ ]);
});
//# sourceMappingURL=index.js.map