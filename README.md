# Language Module react redux

Expose a Data Source and Data Provider for handling different UI translations, leveraging webpack bundle-loader.  
This module is part of the [I-REACT Open Core](https://mobilesolutionsismb.bitbucket.io/i-react-open-core/).

## Acknowledgment

This work was partially funded by the European Commission through the [I-REACT project](http://www.i-react.eu/) (H2020-DRS-1-2015), grant agreement n.700256.

### Build

```bash
    npm i
    npm run build
    npm run build-debug
```

Postinstall script coming soon!

#### Run the example

```bash
    cd example
    npm i
    npm link ../
    npm start
```

## Usage

By default it ships with a sample `locale` folder. Supported languages are in `locale/languages.json` and must correspond to their `lang_<code>` counterparts.

Translations are in a single-level file named `translations.json`. Keys should be underscored so that automatic translators should ignore them.

### Configure webpack

In order to use your own `locale` folder with translations you need to modify your `webpack.config.file`, supposing the folder with your translations `src/locale/`

```javascript
const DEFINITIONS = new webpack.DefinePlugin({
  //..other stuff you may need

  //This tells which languages are supported by this app
  SUPPORTED_LANGUAGES: JSON.stringify(require("./src/locale/languages.json"))

  //..other stuff you may need
});
```

### Dictionary: Localization Proxy

This is the translation engine, an object that translates strings based on a dictionary, falling back to the key itself if it is missing.
It also supports [sprintf-js](https://github.com/alexei/sprintf.js) templates.

```javascript
import { makeDictionary } from "ioc-localization";

//Define translations using plain objects
//better if keys are defined starting with _ (easier to translate on the web)
//Invalid keys 'language', 'translations', 'supportedLanguages', 'isEmpty'
let english = {
  _ok: "OK",
  _cancel: "Cancel",
  _my_name: "My name is %s"
};

let italiano = {
  _ok: "Va bene",
  _cancel: "Annulla",
  _my_name: "Il mio nome è %s"
};

//Factory function takes 3 arguments: supported languages, current language, current dictionary
let dict = makeDictionary(["en", "it"], "en", english);
//Alternatively with 2 args
let dict2 = makeDictionary(["en", "it"], {
  locale: "en",
  translations: english
});

dict.language; // --> {locale: 'en', translations:{...terms}
dict.locale; //--> current locale (2character, e.g. 'en'˙)
dict.supportedLanguages; //--> get supported languages ['en', 'it', ...]
dict.translations; //--> get whole dictionary loaded: {_key: value}
dict.isEmpty; // bool: if translations is empty
dict._ok; //--> 'OK'
dict("_ok"); //--> 'OK'
dict._my_name; //--> 'My name is %s'
dict("_my_name", "John"); //--> 'My name is John'
dict._key_that_does_not_exist; //--> '_key_that_does_not_exist'
//Switch language (locale must be a supported one)
dict.language = { locale: "it", translations: italiano };
dict("_my_name", "John"); //--> 'Il mio nome è John'
dict.language = {
  locale: "es",
  translations: {
    /* spanish terms*/
  }
}; //--> will stay to italian
```

### Reducer

```javascript
import { Reducer } from "ioc-localization";
```

### Data Provider

```javascript
import { LocaleProvider } from "ioc-localization";

let myHoc = LocaleProvider(<MySimpleComponent />);
```

## Props exposed by this module

**locale** _String_ current locale code

**dictionary** _Proxy_ currently selected dictionary

```javascript
this.props.locale; //--> current locale (2character, e.g. 'en'˙)
this.props.supportedLanguages; //--> ['en', 'it', ...]
this.props.dictionary; //--> whole dictionary: see Dictionary above
this.props.isEmpty; // bool: if translations is empty

//Supposing dictionary["_my_name"]==="My name is %s" and name="John"
//Support placeholders (see [sprintf](https://github.com/alexei/sprintf.js))
this.props.dictionary._my_name; // "My name is %s"
this.props.dictionary("_my_name"); // "My name is %s"
this.props.dictionary("_my_name", name); // --> "My name is John"
this.props.dictionary._key_that_does_not_exist; // "_key_that_does_not_exist"
this.props.dictionary("_key_that_does_not_exist"); // "_key_that_does_not_exist"
```

**supportedLanguages** _Array_ list of supported locales codes

**loadDictionary** _Function_ action for loading next dictionary

```javascript
//This example leverages webpack bundle loader
//this.props.loadDictionary(locale:string, loader: function);
//loader returns a PJO of translations
this.props.loadDictionary(
  this.props.locale,
  require(`bundle-loader?lazy!locale/lang_${
    this.props.locale
  }/translation.json`)
);
```
